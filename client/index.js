import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import ApolloClient from 'apollo-client';
import { Router, Route, hashHistory, IndexRoute } from 'react-router'

import App from "./components/App";
import SongList from './components/SongList'
import CreateSong from './components/CreateSong'

const client = new ApolloClient({});

const Root = () => {
  return (
    <ApolloProvider client={client}>
      <Router history={hashHistory} >
        <Route component={App} path="/">
          <IndexRoute component={SongList} />
          <Route component={CreateSong} path="/song/new"/>
        </Route>
      </Router>
    </ApolloProvider>
  )
};

ReactDOM.render(
    <Root />,
  document.querySelector('#root')
);
