import React from 'react'
import gql from 'graphql-tag'
import { graphql } from 'react-apollo'
import { Link, hashHistory } from 'react-router'

class CreateSong extends React.Component {
  constructor(props) {
    super(props)
    this.state = { title: '' }
  }

  handleSubmit() {
    const { title } = this.state;
    console.log('title', title);
    this.props.mutate({ variables: { title } })
      .then(() => hashHistory.push('/'))
  }

  render() {
    return (
      <div>
        <Link to='/'>Back</Link>
        <h3>Create Song</h3>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <input
            onChange={(event) => {
              this.setState({ title: event.target.value })
            }}
            value={this.state.title}
          />
        </form>
      </div>
    )
  }
}

const mutation = gql`
  mutation AddSong($title: String){
    addSong(title: $title) {
      id
      title
    }
  }
`

export default graphql(mutation)(CreateSong)