/**
 * Created by chakrihacker on 11/6/17.
 */
import React, {Component} from 'react'
import {graphql} from 'react-apollo'
import gql from 'graphql-tag'

class SongList extends Component {
  renderSongs() {
    return this.props.data.songs.map(({id, title}) => <li className="collection-item" key={id}>{title}</li>)
  }
  render() {
    console.log(this.props);
    if(this.props.data.loading) { return (<div>Loading...</div>)}
    return (
      <ul className="collection">{this.renderSongs()}</ul>
    )
  }
}
const query = gql`
  {
  songs{
    id
    title
  }
}
`;

export default graphql(query)(SongList);